CREATE TABLE peers (
    info_hash varchar(20) NOT NULL,
    peer_id varchar(20) NOT NULL,
    compact varchar(6) NOT NULL,
    ip INET NOT NULL,
    port INT CHECK (port > 0 AND port < 32768) NOT NULL,
    state boolean NOT NULL,
    updated_at timestamp NOT NULL,
    PRIMARY KEY(info_hash, peer_id)
);

CREATE TABLE tasks (
    name varchar(25) NOT NULL,
    value int NOT NULL
);

